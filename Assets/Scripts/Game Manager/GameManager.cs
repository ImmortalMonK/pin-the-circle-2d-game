﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

	public static GameManager instance;

	private Button shootButton;

	[SerializeField]
	private GameObject needle;
	private GameObject[] gameNeedles;
	public int timer;
	[SerializeField]
	private int howManyNeedles;
	private float needleDistance = 1f;
	private int needleIndex;

	// Use this for initialization
	void Awake () {
		if (instance == null) {
			instance = this;
		}
		GetButton ();
	}
	
	// Update is called once per frame
	void Start () {
		CreateNeedles ();
	
	}

	void GetButton(){
		shootButton = GameObject.Find ("Shoot Button").GetComponent<Button> ();//Пропись кнопки
		shootButton.onClick.AddListener(()=>ShootTheNeedle());
	}
	public void ShootTheNeedle(){
		//Debug.Log ("We are touching the button");// одна из функций проверки нажатия на кнопку
		gameNeedles [needleIndex].GetComponent<NeedleMovementScript> ().FireTheNeedle ();
		needleIndex++;
		if (needleIndex == gameNeedles.Length) {
			Debug.Log ("The game is finished");
			//gameoveractive = true;
			shootButton.onClick.RemoveAllListeners ();
		}

	}
	void CreateNeedles(){// функция создания иголок
		gameNeedles = new GameObject[howManyNeedles];
		Vector3 temp = transform.position;

		for (int i = 0; i < gameNeedles.Length; i++) {
			gameNeedles[i] = Instantiate(needle, temp, Quaternion.identity) as GameObject;
			temp.y -= needleDistance;
		}
	}
	public void InstantiateNeedle(){
		Instantiate (needle, transform.position, Quaternion.identity);
	}
}
