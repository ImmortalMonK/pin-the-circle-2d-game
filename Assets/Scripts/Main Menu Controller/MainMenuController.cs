﻿using UnityEngine;
using System.Collections;

public class MainMenuController : MonoBehaviour {

	public void PlayGame(){
		Application.LoadLevel ("Gameplay");
}
	public void MainMenu(){
		Application.LoadLevel ("Main Menu");
	}
	public void LevelMenu(){
		Application.LoadLevel ("Level Menu");
	}
	public void LevelMenu1()
	{
		Application.LoadLevel ("Level Menu 1");
	}
	public void LevelMenu2()
	{
		Application.LoadLevel ("Level Menu 2");
	}
	public void LevelMenu3()
	{
		Application.LoadLevel ("Level Menu 3");
	}
	public void QuitGame(){
		Application.Quit ();
	}
}