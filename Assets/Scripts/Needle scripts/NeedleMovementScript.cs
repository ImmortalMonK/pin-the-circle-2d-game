﻿using UnityEngine;
using System.Collections;

public class NeedleMovementScript : MonoBehaviour {

	[SerializeField]// один из способов получения компонентов игрового объекта
	private GameObject needleBody;
	private bool canFireNeedle;
	private bool touchedTheCircle;

	private float forceY = 25f;

	private Rigidbody2D myBody;

	void Awake() {
		Initialize ();
		//FireTheNeedle ();
	}

	void Initialize() {
	needleBody.SetActive (false);
		myBody = GetComponent<Rigidbody2D> ();
	}

	void Update() {
		if (canFireNeedle) {
			myBody.velocity = new Vector2 (0, forceY);// придаем движение игле
		}
	}

	public void FireTheNeedle() {
		needleBody.SetActive (true);
		myBody.isKinematic = false;
		canFireNeedle = true;
	}
	void OnTriggerEnter2D(Collider2D target) // функция, которая будет вызываться, когда мы проверяем соприкосновение с другим объектом
	{
		if (touchedTheCircle)// проверка, если мы касаемся круга,
			return;// тогда выходим из ээтой функции
		if (target.tag == "Circle") {
			canFireNeedle = false;
			touchedTheCircle = true;
			myBody.isKinematic = true;
			gameObject.transform.SetParent (target.transform);// делаем иголку субъектом объекта круг, чтобы они взаимодействовали друг с другом
//			if(GameManager.instance !=null){    //способ добавления иголок. проверяет, если объект не пустой добавляем иголку
//				GameManager.instance.InstantiateNeedle ();
//
//			}

			if (ScoreManager.instance != null)
				ScoreManager.instance.SetScore ();
		}
	}
}
