﻿using UnityEngine;
using System.Collections;

public class CircleRotateScript : MonoBehaviour {


	[SerializeField]
	private float rotationSpeed;

	private bool canRotate;// функция позволит нам либо вращать объект либо нет

	private float angle;
	private string game;
	// Use this for initialization
	void Awake () {
		canRotate = true;
		StartCoroutine (ChangeRotation ());
	}
	
	// Update is called once per frame
	void Update () {
		if (canRotate) {
			RotateTheCircle ();
		}
	}
	IEnumerator ChangeRotation(){
		
	yield return new WaitForSeconds (2f);// ждем определенное количество секунд, после выполняется код ниже

	//	rotationSpeed = Random.Range (50, 100);// скорость вращения в пределах от 50 до 100
	//	rotationSpeed *= -1;// обратное вращение

				if (Random.Range (0, 2) > 0) {
					rotationSpeed = -Random.Range (100, 150);
				} else {
					rotationSpeed = Random.Range (100, 150);
				}

	StartCoroutine(ChangeRotation());// вызываем функцию для следующего ее выполнения
	}
	void RotateTheCircle(){

		angle = transform.rotation.eulerAngles.z;// показываем что нам надо вращать по плоскости зед
		angle += rotationSpeed*Time.deltaTime;// угол поворота + скорость вращения за определенную единицу времени
		//Time.deltatime -  это время которое потребовалось для прохождения последнего кадра. 
		//Его стоит использовать только для определения величины в одном кадре которую нужно изменять, 
		//чтобы например плавно переместить объект. 
		//Для измерения отрезков времени нужно использовать Time.time - это время прошедшее с начала игры,
		//то есть любую паузу можно выдержать благодаря Time.time. 
		transform.rotation = Quaternion.Euler(new Vector3(0,0,angle));
	}
}
